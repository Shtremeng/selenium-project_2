import time
from datetime import time

from selenium.webdriver.chrome.webdriver import WebDriver
# from selenium.webdriver.remote.webelement import WebElement
from telegram import Bot
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from echo.config import TG_TOKEN


def test_site_error_search():
    while True:
        seconds_left: int = 120
        driver: WebDriver = WebDriver(executable_path='C://selenium//chromedriver.exe')
        driver.get('https://delikateska.ru/?utm_source=test_200')
        assert driver.title == 'Интернет магазин продуктов и деликатесов — купить продукты онлайн с доставкой на дом'
        time.sleep(30)

def do_start(bot: Bot, update: Update):
    bot.send_message(chat_id=update.message.chat_id, text="while")

def main():
    bot: Bot = Bot(token=TG_TOKEN)
    updater = Updater(bot=bot)
    start_handler = CommandHandler("start", do_start())
    message_handler = MessageHandler(Filters.text, do_echo)
    updater.dispatcher.add_handler(start_handler)
    updater.dispatcher.add_handler(message_handler)
    updater.start_polling()
    updater.idle()
    # search_results: WebElement = driver.find_elements_by_xpath('//div[@id="mainMenu"]')
    # link = search_results[0].find_element_by_xpath('//a[@id="cabMenuItem"]')
    # link.click()
    # search_input: WebElement = driver.find_element_by_xpath('//input[@id="login_email"]')
    # search_input.send_keys('vlad.miroshnikov.92@mail.ru')
    # search_input: WebElement = driver.find_element_by_xpath('//input[@id="login_pass"]')
    # search_input.send_keys('marionetka92')
    # search_button = driver.find_element_by_xpath('//p[@class="submit forgotpass-ctn"]//input[@type="submit"]')
    # search_button.click()